<?php

/* Start session */
session_start();

if (!isset($_SESSION['errors']))
{
    $_SESSION['errors'] = [];
}

if (!isset($_SESSION['totalSum']))
{
    $_SESSION['totalSum'] = 0;
}

if (!isset($_SESSION['cart']))
{
    $_SESSION['cart'] = [];
}

if (isset($_GET['logout']))
{
    if ($_SESSION['logIn']['status'] == 1)
    {
        session_unset();
        session_destroy();
        $http = new Http();
        $http->redirectTo("/");
    }
}

function dd($what)
{
    echo '<pre>';
    die(print_r($what));
    echo '</pre>';
}

function stripTags(array $data)
{
    function stripTag(&$item)
    {
        $item = strip_tags($item);
    }
    array_walk($data, 'stripTag');
    return $data;
}

function logInSession(array $userData)
{
    $_SESSION['logIn'] = $userData;
}