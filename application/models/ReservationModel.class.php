<?php

class ReservationModel
{
    public function addReservation($data)
    {
        /* Create DB connection */
        $database = new Database();
        /* Prepare insertion into user table */
        if (!isset($_SESSION['logIn']))
        {
            $sqlInsertUser = 'INSERT INTO user(firstName, phone) VALUES(?, ?);';
            $database->executeSql($sqlInsertUser, [
                $data['name'],
                $data['phone'],
            ]);

            /* Prepare query to get max value of user Id */
            $sqlMaxUserId = 'SELECT MAX(Id) AS user_Id FROM user;';
            $userId = $database->queryOne($sqlMaxUserId);
            $userId = $userId['user_Id'];
        }
        else if (isset($_SESSION['logIn']) && $_SESSION['logIn']['status'] == 1)
        {
            $userId = $_SESSION['logIn']['Id'];
        }

        $dateSeparately = [
            $data['year'],
            $data['month'],
            $data['day'],
        ];
        $fullDate = join('-', $dateSeparately);

        $timeSeparately = [
            $data['hour'],
            $data['minutes'],
        ];
        $fullTime = join(':', $timeSeparately);

        /* Prepare insertion into bookings table */
        $sqlInBooking = 'INSERT INTO booking(bookingDate, bookingTime, numberOfSeats, user_id) VALUES(?, ?, ?, ?);';
        $database->executeSql($sqlInBooking, [
            $fullDate,
            $fullTime,
            $data['persons'],
            $userId,
        ]);
    }
}