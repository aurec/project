<?php

class LoginModel
{
    public function logInUser($data, $http)
    {
        /* Create DB connection */
        $database = new Database();
        /* Prepare query to check if user exists */
        $sql = 'SELECT * FROM user 
                WHERE email = "' . $data['email'] . '" AND 
                      password = "' . $data['password'] . '" AND
                      status = 1;';
        $userExists = $database->queryOne($sql);

        if ($userExists == null)
        {
            $_SESSION['errors']['no_user'] = 'Incorrect user email or password...';
        }
        else
        {
            ini_set('date.timezone', 'Europe/Vilnius');
            $updateLastLogin = 'UPDATE user SET lastLoginTimestamp = "' . date("Y-m-d H:i:s") . '"
                                WHERE Id = ' . $userExists['Id'] . ';';
            $database->query($updateLastLogin);
            logInSession($userExists);
            $http->redirectTo("/");
        }
    }
}