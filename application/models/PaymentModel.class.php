<?php

class PaymentModel
{
    public function createOrderPayment()
    {
        /* Create DB connection */
        $database = new Database();
        /* Prepare insertion into order table */
        $sqlInsertOrder = 'INSERT INTO restaurant.order(user_Id, totalAmount, taxAmount)
                           VALUES(?, ?, ?);';
        $taxAmount = $_SESSION['totalSum'] - round(($_SESSION['totalSum'] * 100) / 121, 2);
        $database->executeSql($sqlInsertOrder, [
            $_SESSION['logIn']['Id'],
            $_SESSION['totalSum'],
            $taxAmount,
        ]);

        $sql = 'SELECT MAX(Id) AS max_meal_Id FROM restaurant.order;';
        $maxMealId = $database->queryOne($sql);

        $sqlInsertOrderLine = 'INSERT INTO orderline(quantityOrdered, meal_Id, order_Id, priceEach)
                               VALUES(?, ?, ?, ?);';
        for ($i = 0; $i < count($_SESSION['cart']); $i++)
        {
            $database->executeSql($sqlInsertOrderLine, [
                $_SESSION['cart'][$i]['quantity'],
                $_SESSION['cart'][$i]['product_id'],
                $maxMealId['max_meal_Id'],
                $_SESSION['cart'][$i]['price_each'],
            ]);
        }

        $_SESSION['totalSum'] = 0;
        unset($_SESSION['cart']);
    }
}