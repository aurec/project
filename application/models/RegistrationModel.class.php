<?php

class RegistrationModel
{
    public function createUser($data, $http)
    {
        /* Create DB connection */
        $database = new Database();
        /* Prepare insertion into user table */
        $sqlInsertUser = 'INSERT INTO user(status, firstName, lastName, email, password, city, address, phone)
                          VALUES(?, ?, ?, ?, ?, ?, ?, ?);';
        $database->executeSql($sqlInsertUser, [
            1,
            $data['first-name'],
            $data['last-name'],
            $data['email'],
            $data['password'],
            $data['city'],
            $data['address'],
            $data['phone'],
        ]);

        $sqlToMaxId = 'SELECT MAX(Id) AS max_Id FROM user';
        $maxId = $database->queryOne($sqlToMaxId);

        $sql = 'SELECT * FROM user WHERE Id = ' . $maxId['max_Id'] . ';';
        $createdUser = $database->queryOne($sql);
        logInSession($createdUser);
        $http->redirectTo("/");
    }

    public function updateUser($data)
    {
        /* Create DB connection */
        $database = new Database();
        /* Prepare updates on user table */
        if ($_SESSION['logIn']['status'] == 1)
        {
            $sqlUpdateUser = 'UPDATE user SET firstName = "' . $data['first-name'] . '",
                                              lastName = "' . $data['last-name'] . '",
                                              phone = "' . $data['phone'] . '",
                                              email = "' . $data['email'] . '",
                                              password = "' . $data['password'] . '",
                                              city = "' . $data['city'] . '",
                                              address = "' . $data['address'] . '"
                                        WHERE Id = ' . $_SESSION['logIn']['Id'] . ';';
            $database->query($sqlUpdateUser);
        }

        $sql = 'SELECT * FROM user 
                WHERE Id = ' . $_SESSION['logIn']['Id'] . ';';
        $updatedUser = $database->queryOne($sql);
        logInSession($updatedUser);
    }

    public function orUserIsTaken($email)
    {
        /* Create DB connection */
        $database = new Database();

        /* Prepare query to check if user is taken */
        $sql = 'SELECT email FROM user 
                WHERE email = "' . $email . '"
                AND status = 1;';
        $userExists = $database->queryOne($sql);

        if (!isset($_SESSION['logIn']))
        {
            /* Check if query's result is NULL */
            if ($userExists != null)
            {
                $_SESSION['errors']['user_taken'] = 'That user is taken...';
            }
            else
            {
                unset($_SESSION['errors']['user_taken']);
            }
        }
        elseif ($_SESSION['logIn']['status'] == 1)
        {
            if ($userExists['email'] == $_SESSION['logIn']['email'])
            {
                unset($_SESSION['errors']['user_taken']);
            }
            else
            {
                $_SESSION['errors']['user_taken'] = 'That user is taken...';
            }
        }
    }
}