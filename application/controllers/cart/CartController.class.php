<?php

class CartController
{
    public function httpGetMethod(Http $http, array $queryFields)
    {
        /*
         * Called as an HTTP GET request
         *
         * The argument $http is an object to make redirects etc.
         * The argument $queryFields contains the equivalent of $_GET native PHP.
         */
        /* Create model's object */
        $productsModel = new ProductsModel();
        $cartModel = new CartModel();

        /* Call model's function getProducts() */
        $products = $productsModel->getProducts();
        $cartModel->createCart($products, $_SESSION['cart']);

        return [
            'cart'=>$_SESSION['cart'],
        ];
    }

    public function httpPostMethod(Http $http, array $formFields)
    {
        /*
         * Called as an HTTP POST request
         *
         * The argument $http is an object to make redirects etc.
         * The argument $formFields contains the equivalent of $_POST native PHP.
         */
        /* Create model's object*/
        $productsModel = new ProductsModel();
        $cartModel = new CartModel();

        /* Call model's function getProducts() */
        $products = $productsModel->getProducts();
        $cartModel->createCart($products, $_SESSION['cart']);

        if (isset($formFields['mode']) && $formFields['mode'] == 'ajax')
        {
            $id = (int)$formFields['product_id'];

            for ($i = 0; $i < count($_SESSION['cart']); $i++)
            {
                if ($_SESSION['cart'][$i]['product_id'] == $id)
                {
                    $_SESSION['totalSum'] -= number_format(($_SESSION['cart'][$i]['quantity']*$_SESSION['cart'][$i]['price_each']), 2);
                    array_splice($_SESSION['cart'], $i, 1);
                    break;
                }
            }
        }

        /* Return to AJAX */
        $http->sendJsonResponse([
            'success'=>'success',
            'sum'=> $_SESSION['totalSum'],
        ]);
    }
}