<?php

class PaymentController
{
    public function httpGetMethod(Http $http, array $queryFields)
    {
        /*
         * Called as an HTTP GET request
         *
         * The argument $http is an object to make redirects etc.
         * The argument $queryFields contains the equivalent of $_GET native PHP.
         */
        $paymentModel = new PaymentModel();
        $paymentModel->createOrderPayment($http);
        header('refresh:5; url=' . $_SERVER['SCRIPT_NAME']);
    }

    public function httpPostMethod(Http $http, array $formFields)
    {
        /*
         * Called as an HTTP POST request
         *
         * The argument $http is an object to make redirects etc.
         * The argument $formFields contains the equivalent of $_POST native PHP.
         */
    }
}