<?php

class HomeController
{
    public function httpGetMethod(Http $http, array $queryFields)
    {
        /*
         * Called as an HTTP GET request
         *
         * The argument $http is an object to make redirects etc.
         * The argument $queryFields contains the equivalent of $_GET native PHP.
         */
        /* Create model's object*/
        $productsModel = new ProductsModel();
        /* Call model's function getProducts() */
        $products = $productsModel->getProducts();
//        dd($products);
        return [
            'products'=>$products,
        ];
    }

    public function httpPostMethod(Http $http, array $formFields)
    {
        /*
         * Called as an HTTP POST request
         *
         * The argument $http is an object to make redirects etc.
         * The argument $formFields contains the equivalent of $_POST native PHP.
         */
        if (isset($formFields['mode']) && $formFields['mode'] == 'ajax')
        {
            $_SESSION['totalSum'] += number_format(($formFields['quantity'] * $formFields['price_each']), 2);

            if (empty($_SESSION['cart']))
            {
//                $formFields['price_each'] *= $formFields['quantity'];
                $_SESSION['cart'][] = $formFields;
            }
            else
            {
                $isInCart = false;
                for ($i = 0; $i < count($_SESSION['cart']); $i++)
                {
                    if ($_SESSION['cart'][$i]['product_id'] == $formFields['product_id'])
                    {
                        $_SESSION['cart'][$i]['quantity'] += $formFields['quantity'];
//                        $_SESSION['cart'][$i]['price_each'] += ($formFields['quantity'] * $formFields['price_each']);
                        $isInCart = true;
                        break;
                    }
                }
                if (!$isInCart)
                {
//                    $formFields['price_each'] *= $formFields['quantity'];
                    $_SESSION['cart'][] = $formFields;
                }
            }

            /* Return to AJAX */
            $http->sendJsonResponse([
                'sum'=>$_SESSION['totalSum'],
            ]);
        }
        /* Create model's object*/
        $productsModel = new ProductsModel();
        /* Call model's function getProducts() */
        $products = $productsModel->getProducts();

        return [
            'products'=>$products,
        ];
    }
}