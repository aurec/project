<?php

class RegistrationController
{
    public function httpGetMethod(Http $http, array $queryFields)
    {
        /*
         * Called as an HTTP GET request
         *
         * The argument $http is an object to make redirects etc.
         * The argument $queryFields contains the equivalent of $_GET native PHP.
         */
    }

    public function httpPostMethod(Http $http, array $formFields)
    {
        /*
         * Called as an HTTP POST request
         *
         * The argument $http is an object to make redirects etc.
         * The argument $formFields contains the equivalent of $_POST native PHP.
         */
        $dataWithoutTags = stripTags($formFields);

        $registration = new RegistrationModel();
        $registration->orUserIsTaken($dataWithoutTags['email']);
        if (!isset($_SESSION['errors']['user_taken']))
        {
            if (!isset($_SESSION['logIn']))
            {
                $registration->createUser($dataWithoutTags, $http);
            }
            elseif ($_SESSION['logIn'])
            {
                $registration->updateUser($dataWithoutTags);
            }
        }
    }
}