<?php

class LoginController
{
    public function httpGetMethod(Http $http, array $queryFields)
    {
        /*
         * Called as an HTTP GET request
         *
         * The argument $http is an object to make redirects etc.
         * The argument $queryFields contains the equivalent of $_GET native PHP.
         */
    }

    public function httpPostMethod(Http $http, array $formFields)
    {
        /*
         * Called as an HTTP POST request
         *
         * The argument $http is an object to make redirects etc.
         * The argument $formFields contains the equivalent of $_POST native PHP.
         */
        $dataWithoutTags = stripTags($formFields);

        $login = new LoginModel();
        $login->logInUser($dataWithoutTags, $http);
    }
}