'use strict';

$(document).ready(function() {
    $('.add-item').removeClass('hidden');
    $('.add-item').hide();

    $('#add-item').click(function() {
        if($('#form-action').val() != 'edit') {
            if($('.add-item').is(':visible')) {
                $('.add-item').hide();
            } else {
                $('.add-item').show();
            }
        } else {
            $('.add-item').show();
            $('.add-item')[0].reset();
        }
        $('#form-action').val('add');
        $('#add').show();
        $('#edit').addClass('hidden');
    });

    $('.btn-edit-line').click(function(event) {
        event.preventDefault();
        $('.add-item').show();
        $('#add').hide();
        $('#edit').removeClass('hidden');
        $('#form-action').val('edit');

        var url = 'product';
        var product_data = {
            product_id: $(this).data('product_id'),
            mode:       'ajax'
        };

        $.ajax({
            url: url,
            type: 'POST',
            data: product_data,
            dataType: 'json',
            error: function(error) {
                console.log(error);
                alert('AJAX error');
            },
            success: function(data) {
                console.log(data);
                $('#product-id').val(data.id);
                $('#product-name').val(data.name);
                $('#product-description').val(data.description);
                $('#quantity-in-stock').val(data.quantityInStock);
                $('#buy-price').val(data.buyPrice);
                $('#sale-price').val(data.salePrice);
            }
        });
    });

    $('.btn-add-to-cart').click(function(event) {
        event.preventDefault();
        var url = '';

        var product_data = {
            product_id: $(this).data('product_id'),
            quantity:   $(this).parent().parent().find('.quantity').val(),
            price_each: $(this).data('price_each'),
            mode:       'ajax'
        };

        $.ajax({
            url: url,
            type: 'POST',
            data: product_data,
            dataType: 'json',
            error: function(error) {
                console.log(error);
                alert('AJAX error');
            },
            success: function(data) {
                $('.cart-total').html(data.sum + ' €');
            }
        });
    });

    $('.btn-delete-from-cart').click(function(event) {
        event.preventDefault();
        var url = 'cart';
        var clickTarget = $(this);

        var cart_product_data = {
            product_id: $(this).data('product_id'),
            mode:    'ajax'
        };

        $.ajax({
            url: url,
            type: 'POST',
            data: cart_product_data,
            // dataType: 'json',
            error: function(error){
                alert('AJAX error');
                alert('AJAX error');
            },
            success: function(data) {
                var newData = JSON.parse(data);
                clickTarget.parent().parent().remove();
                $('.cart-total').html(newData.sum + ' €');
                if($('.meal-list tr').length == 3) {
                    $('.meal-list').addClass('hidden');
                    $('.cart-empty').removeClass('hidden');
                }
            }
        });
    });
});

var password = document.getElementById("password");
var confirm_password = document.getElementById("confirm_password");

if (password && confirm_password)
{
    password.onchange = validatePassword;
    confirm_password.onkeyup = validatePassword;
}

/////////////////////////////////////////////////////////////////////////////////////////
// FUNCTIONS                                                                           //
/////////////////////////////////////////////////////////////////////////////////////////
/*
 * Function to check if passwords match.
 */
function validatePassword()
{
    if(password.value != confirm_password.value)
    {
        confirm_password.setCustomValidity("Passwords Don't Match");
    }
    else
    {
        confirm_password.setCustomValidity('');
    }
}

/*
 * Function to check if there are empty fields in form.
 */
function validateForm()
{
    var isValid = true;
    var elements = document.getElementById('generic-form').getElementsByTagName('input');

    for (var i = 0; i < elements.length; i++)
    {
        if (elements[i].value.length < 1)
        {
            isValid = false;
        }
    }

    if (isValid)
    {
        document.getElementById('generic-form').submit();
    }
    else
    {
        document.getElementById('error-message').classList.remove('hidden');
        document.getElementById('error-message').innerHTML = '* Please fill all required fields...';
        return isValid;
    }
}

/////////////////////////////////////////////////////////////////////////////////////////
// CODE PRINCIPAL                                                                      //
/////////////////////////////////////////////////////////////////////////////////////////

