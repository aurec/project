-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: 2016 m. Rgp 29 d. 23:12
-- Server version: 10.1.13-MariaDB
-- PHP Version: 7.0.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `restaurant`
--

-- --------------------------------------------------------

--
-- Sukurta duomenų struktūra lentelei `booking`
--

CREATE TABLE `booking` (
  `Id` int(11) NOT NULL,
  `bookingDate` date NOT NULL,
  `bookingTime` time NOT NULL,
  `numberOfSeats` tinyint(4) NOT NULL,
  `user_Id` int(11) NOT NULL,
  `creationTimestamp` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Sukurta duomenų kopija lentelei `booking`
--

INSERT INTO `booking` (`Id`, `bookingDate`, `bookingTime`, `numberOfSeats`, `user_Id`, `creationTimestamp`) VALUES
(1, '2016-08-29', '10:00:00', 1, 34, '0000-00-00 00:00:00'),
(2, '2016-08-29', '10:00:00', 1, 34, '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Sukurta duomenų struktūra lentelei `meal`
--

CREATE TABLE `meal` (
  `Id` int(11) NOT NULL,
  `name` varchar(30) NOT NULL,
  `photo` varchar(30) NOT NULL,
  `description` varchar(250) NOT NULL,
  `quantityInStock` tinyint(4) NOT NULL,
  `buyPrice` double NOT NULL,
  `salePrice` double NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Sukurta duomenų kopija lentelei `meal`
--

INSERT INTO `meal` (`Id`, `name`, `photo`, `description`, `quantityInStock`, `buyPrice`, `salePrice`) VALUES
(1, 'Cheeseburger', '1.png', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod\r\ntempor incididunt ut labore et dolore magna aliqua.', 72, 1, 3),
(2, 'Cheeseburger', '2.png', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod\r\ntempor incididunt ut labore et dolore magna aliqua.', 18, 1, 3.5),
(3, 'Cheeseburger', '3.png', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod\r\ntempor incididunt ut labore et dolore magna aliqua.', 14, 3, 6),
(4, 'Cheeseburger', '4.png', 'Le carrot cake maison ravira les plus gourmands et les puristes : tous les ingrédients sont naturels !\r\nÀ consommer sans modération', 9, 3, 6.75),
(5, 'Cheeseburger', '5.png', 'Les donuts sont fabriqués le matin même et sont recouvert d''une délicieuse sauce au chocolat !', 16, 3, 6.2),
(6, 'French Fries', '6.png', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod\r\ntempor incididunt ut labore et dolore magna aliqua.', 16, 3, 6.2),
(7, 'French Fries', '7.png', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod\r\ntempor incididunt ut labore et dolore magna aliqua.', 12, 2, 5.35),
(8, 'French Fries', '8.png', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod\r\ntempor incididunt ut labore et dolore magna aliqua.', 72, 0.6, 3);

-- --------------------------------------------------------

--
-- Sukurta duomenų struktūra lentelei `order`
--

CREATE TABLE `order` (
  `Id` int(11) NOT NULL,
  `user_Id` int(11) NOT NULL,
  `totalAmount` double NOT NULL,
  `taxRate` float NOT NULL DEFAULT '21',
  `taxAmount` double NOT NULL,
  `creationTimestamp` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `completeTimestamp` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Sukurta duomenų kopija lentelei `order`
--

INSERT INTO `order` (`Id`, `user_Id`, `totalAmount`, `taxRate`, `taxAmount`, `creationTimestamp`, `completeTimestamp`) VALUES
(1, 34, 8.5, 21, 1.48, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2, 34, 3, 21, 0.52, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3, 34, 3, 21, 0.52, '2016-08-29 12:51:36', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Sukurta duomenų struktūra lentelei `orderline`
--

CREATE TABLE `orderline` (
  `Id` int(11) NOT NULL,
  `quantityOrdered` int(4) NOT NULL,
  `meal_Id` int(11) NOT NULL,
  `order_Id` int(11) NOT NULL,
  `priceEach` double NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Sukurta duomenų kopija lentelei `orderline`
--

INSERT INTO `orderline` (`Id`, `quantityOrdered`, `meal_Id`, `order_Id`, `priceEach`) VALUES
(1, 1, 1, 1, 3),
(2, 1, 2, 1, 5.5),
(3, 1, 1, 2, 3),
(4, 1, 1, 3, 3);

-- --------------------------------------------------------

--
-- Sukurta duomenų struktūra lentelei `user`
--

CREATE TABLE `user` (
  `Id` int(11) NOT NULL,
  `firstName` varchar(40) NOT NULL,
  `lastName` varchar(40) NOT NULL,
  `email` varchar(50) NOT NULL,
  `password` varchar(64) NOT NULL,
  `address` varchar(250) NOT NULL,
  `city` varchar(40) NOT NULL,
  `phone` char(10) NOT NULL,
  `creationTimestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `lastLoginTimestamp` datetime NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Sukurta duomenų kopija lentelei `user`
--

INSERT INTO `user` (`Id`, `firstName`, `lastName`, `email`, `password`, `address`, `city`, `phone`, `creationTimestamp`, `lastLoginTimestamp`, `status`) VALUES
(6, 'md5', 'md5', 'md5@md5.lt', '1bc29b36f623ba82aaf6724fd3b16718', '', '', '111 11 111', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(7, 'sha256', 'sha256', 'sha256@sha256.lt', '5d5b09f6dcb2d53a5fffc60c4ac0d55fabdf556069d6631545f42aa6e3500f2e', '', '', '555 55 555', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(31, 'aaa', 'aaa', 'aaa@aaa.lt', 'aaa', '', '', '555', '2016-08-22 12:18:46', '0000-00-00 00:00:00', 0),
(32, 'asdasd', 'sadasda', 'sdasd', '5fd924625f6ab16a19cc9807c7c506ae1813490e4ba675f843d5a10e0baacdb8', '', '', 'asdasd', '2016-08-23 08:00:21', '0000-00-00 00:00:00', 0),
(33, 'sadasd', 'sdfgsdf', 'sdfsdf', '4e878bbf1802b0a08784c36923faf241c86b44531293f190409c2aac37922be4', '', '', 'dsfgsdf', '2016-08-23 08:01:21', '0000-00-00 00:00:00', 0),
(34, 'as', 'as', 'aaas@aaa.lt', '1', 'as', 'as', 'as', '2016-08-29 05:29:37', '2016-08-29 12:55:06', 1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `booking`
--
ALTER TABLE `booking`
  ADD PRIMARY KEY (`Id`),
  ADD KEY `User_Id` (`user_Id`);

--
-- Indexes for table `meal`
--
ALTER TABLE `meal`
  ADD PRIMARY KEY (`Id`),
  ADD KEY `Id` (`Id`);

--
-- Indexes for table `order`
--
ALTER TABLE `order`
  ADD PRIMARY KEY (`Id`),
  ADD KEY `Id` (`Id`),
  ADD KEY `User_Id` (`user_Id`);

--
-- Indexes for table `orderline`
--
ALTER TABLE `orderline`
  ADD PRIMARY KEY (`Id`),
  ADD KEY `Meal_Id` (`meal_Id`),
  ADD KEY `Order_Id` (`order_Id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`Id`),
  ADD KEY `Id` (`Id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `booking`
--
ALTER TABLE `booking`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `meal`
--
ALTER TABLE `meal`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `order`
--
ALTER TABLE `order`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `orderline`
--
ALTER TABLE `orderline`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=35;
--
-- Apribojimai eksportuotom lentelėm
--

--
-- Apribojimai lentelei `booking`
--
ALTER TABLE `booking`
  ADD CONSTRAINT `booking_ibfk_1` FOREIGN KEY (`user_Id`) REFERENCES `user` (`Id`);

--
-- Apribojimai lentelei `order`
--
ALTER TABLE `order`
  ADD CONSTRAINT `order_ibfk_1` FOREIGN KEY (`user_Id`) REFERENCES `user` (`Id`);

--
-- Apribojimai lentelei `orderline`
--
ALTER TABLE `orderline`
  ADD CONSTRAINT `orderline_ibfk_1` FOREIGN KEY (`order_Id`) REFERENCES `order` (`Id`),
  ADD CONSTRAINT `orderline_ibfk_2` FOREIGN KEY (`meal_Id`) REFERENCES `meal` (`Id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
